import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;

import br.fsa.utils.Imagem;
import br.fsa.operacoes.Aritmetica;
import br.fsa.operacoes.Logica;
public class Main {

	public static void main(String[] args) {
		System.out.println(System.getProperty("user.dir"));
		
		JFrame f = new JFrame("Teste");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setBounds(0,0,800,600);
		Imagem a = null;
		Imagem b = null;
		try {
			a = new Imagem(new File("./lena.png"));
			b = new Imagem(new File("./liliana.jpg"));
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		if(a != null && b != null) {
//			Imagem img = Aritmetica.soma(a, b); 
//			Imagem img = Aritmetica.subtracao(a, b);
//			Imagem img = Aritmetica.multiplicacao(a, b); 
			Imagem img = Aritmetica.divisao(a, b); 
//			Imagem img = Aritmetica.blending(a, b, 0.3f);
			
//			Imagem img = Logica.and(a, b);
			f.add(img);
		}
		f.setVisible(true);
		
		
		
	}
}
