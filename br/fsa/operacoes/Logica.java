package br.fsa.operacoes;

import br.fsa.utils.Imagem;

public class Logica {
	
	public static Imagem and(Imagem a, Imagem b) 
	{
		Imagem result = new Imagem(Math.max(a.getW(), b.getW()), Math.max(a.getH(), b.getH()));
		
		for(int j = 0; j < result.getH(); j++)
		{
			for(int i = 0; i < result.getW(); i++)
			{
				//SEPARA OS CANAIS DE COR DE A
				int cora = a.getP(i, j);
				int ra = cora >> 16 & 0xff;
				int ga = cora >> 8 & 0xff;
				int ba = cora & 0xff;
				//SEPARA OS CANAIS DE COR DE B
				int corb = b.getP(i, j);
				int rb = corb >> 16 & 0xff;
				int gb = corb >> 8 & 0xff;
				int bb = corb & 0xff;
				//PARA CADA CANAL FAZ RA & RB
				int resR = ra & rb;
				int resG = ga & gb;
				int resB = ba & bb;
				
				//JUNTA OS RESULTADOS E SALVA O PIXEL NA IMAGEM RESULTANTE
				int corR = resR << 16 | resG << 8 | resB;
				
				result.setP(i, j, corR);
			}
		}
		
		return result;
	}
	
	public static Imagem or(Imagem a, Imagem b) 
	{
		Imagem result = new Imagem(Math.max(a.getW(), b.getW()), Math.max(a.getH(), b.getH()));
		
		for(int j = 0; j < result.getH(); j++)
		{
			for(int i = 0; i < result.getW(); i++)
			{
				//SEPARA OS CANAIS DE COR DE A
				int cora = a.getP(i, j);
				int ra = cora >> 16 & 0xff;
				int ga = cora >> 8 & 0xff;
				int ba = cora & 0xff;
				//SEPARA OS CANAIS DE COR DE B
				int corb = b.getP(i, j);
				int rb = corb >> 16 & 0xff;
				int gb = corb >> 8 & 0xff;
				int bb = corb & 0xff;
				//PARA CADA CANAL FAZ RA & RB
				int resR = ra | rb;
				int resG = ga | gb;
				int resB = ba | bb;
				
				//JUNTA OS RESULTADOS E SALVA O PIXEL NA IMAGEM RESULTANTE
				int corR = resR << 16 | resG << 8 | resB;
				
				result.setP(i, j, corR);
			}
		}
		
		return result;
	}
	
	public static Imagem xor(Imagem a, Imagem b) 
	{
		Imagem result = new Imagem(Math.max(a.getW(), b.getW()), Math.max(a.getH(), b.getH()));
		
		for(int j = 0; j < result.getH(); j++)
		{
			for(int i = 0; i < result.getW(); i++)
			{
				//SEPARA OS CANAIS DE COR DE A
				int cora = a.getP(i, j);
				int ra = cora >> 16 & 0xff;
				int ga = cora >> 8 & 0xff;
				int ba = cora & 0xff;
				//SEPARA OS CANAIS DE COR DE B
				int corb = b.getP(i, j);
				int rb = corb >> 16 & 0xff;
				int gb = corb >> 8 & 0xff;
				int bb = corb & 0xff;
				//PARA CADA CANAL FAZ RA & RB
				int resR = ra ^ rb;
				int resG = ga ^ gb;
				int resB = ba ^ bb;
				
				//JUNTA OS RESULTADOS E SALVA O PIXEL NA IMAGEM RESULTANTE
				int corR = resR << 16 | resG << 8 | resB;
				
				result.setP(i, j, corR);
			}
		}
		
		return result;
	}

}
